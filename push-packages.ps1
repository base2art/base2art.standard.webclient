
cd output-packages

$pkgs = Get-ChildItem -Path "." -filter "*.nupkg"

$key = Get-Content -Raw "~/.config/base2art.nuget-config"
$key = $key.Trim()

ForEach ($pkg in $pkgs) 
{
    dotnet nuget push $pkg.Name -k "$($key)" --source https://api.nuget.org/v3/index.json
}

cd ..
