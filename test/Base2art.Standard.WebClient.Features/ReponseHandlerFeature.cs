namespace Base2art.WebClient.Features
{
    using System;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using FluentAssertions;
    using HtmlAgilityPack;
    using ResponseHandlers;
    using ResponseHandlers.Html;
    using ResponseHandlers.Json;
    using Xunit;

    public class ReponseHandlerFeature
    {
        private static async Task<T> DoIt<T>(Func<Action<T>, IResponseHandler> creator, string text)
        {
            var target = default(T);
            var handler = creator(x => target = x);
            await handler.Invoke(text);
            return target;
        }

        private static async Task<T> DoIt<T>(Func<Action<T>, IResponseHandler> creator, byte[] bytes)
        {
            var target = default(T);
            var handler = creator(x => target = x);
            await handler.Invoke(bytes);
            return target;
        }

        private static async Task<T> DoItAsync<T>(Func<Func<T, Task>, IResponseHandler> creator, string text)
        {
            var target = default(T);
            var handler = creator(async x => target = x);
            await handler.Invoke(text);
            return target;
        }

        private static async Task<T> DoItAsync<T>(Func<Func<T, Task>, IResponseHandler> creator, byte[] bytes)
        {
            var target = default(T);
            var handler = creator(async x => target = x);
            await handler.Invoke(bytes);
            return target;
        }

        private class Person
        {
            public string Name { get; set; }
        }

        [Fact]
        public async void ShouldBinary()
        {
            var input = "{Name: 'SjY'}";
            var inputBytes = Encoding.Default.GetBytes(input);

            var value = await DoIt<byte[]>(x => new ByteArrayHandler(x), input);
            value.Should().BeEquivalentTo(inputBytes);

            var valueBytes = await DoIt<byte[]>(x => new ByteArrayHandler(x), inputBytes);
            valueBytes.Should().BeEquivalentTo(inputBytes);

            var valueAsync = await DoItAsync<byte[]>(x => new ByteArrayHandler(x), input);
            valueAsync.Should().BeEquivalentTo(inputBytes);

            var valueBytesAsync = await DoItAsync<byte[]>(x => new ByteArrayHandler(x), inputBytes);
            valueBytesAsync.Should().BeEquivalentTo(inputBytes);
        }

        [Fact]
        public async void ShouldGetJson()
        {
            var input = "{\"Name\":\"SjY\"}";
            var inputBytes = Encoding.Default.GetBytes(input);

            var value = await DoIt<Person>(x => new JsonHandler<Person>(x), input);
            value.Name.Should().Be("SjY");

            var valueBytes = await DoIt<Person>(x => new JsonHandler<Person>(x), inputBytes);
            valueBytes.Name.Should().Be("SjY");

            var valueAsync = await DoItAsync<Person>(x => new JsonHandler<Person>(x), input);
            valueAsync.Name.Should().Be("SjY");

            var valueBytesAsync = await DoItAsync<Person>(x => new JsonHandler<Person>(x), inputBytes);
            valueBytesAsync.Name.Should().Be("SjY");
        }

        [Fact]
        public async void ShouldHtml()
        {
            var assembly = typeof(ReponseHandlerFeature).Assembly;

            var resourceStream = assembly.GetManifestResourceStream("Base2art.WebClient.Features.Fixtures.test.html");
            string input;
            using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
            {
                input = await reader.ReadToEndAsync();
            }

            var inputBytes = Encoding.Default.GetBytes(input);

            var value = await DoIt<HtmlDocument>(x => new HtmlHandler(x), input);
            value.DocumentNode
                 .SelectSingleNode("//*[@class='skillset']/div[6]")
                 .InnerText
                 .Trim().Should().BeEquivalentTo("JWT");

            var valueBytes = await DoIt<HtmlDocument>(x => new HtmlHandler(x), inputBytes);
            valueBytes.DocumentNode
                      .SelectSingleNode("//*[@class='skillset']/div[6]")
                      .InnerText
                      .Trim().Should().BeEquivalentTo("JWT");

            var valueAsync = await DoItAsync<HtmlDocument>(x => new HtmlHandler(x), input);
            valueAsync.DocumentNode
                      .SelectSingleNode("//*[@class='skillset']/div[6]")
                      .InnerText
                      .Trim().Should().BeEquivalentTo("JWT");

            var valueBytesAsync = await DoItAsync<HtmlDocument>(x => new HtmlHandler(x), inputBytes);
            valueBytesAsync.DocumentNode
                           .SelectSingleNode("//*[@class='skillset']/div[6]")
                           .InnerText
                           .Trim().Should().BeEquivalentTo("JWT");
        }

        [Fact]
        public async void ShouldString()
        {
            var input = "{Name: 'SjY'}";
            var inputBytes = Encoding.Default.GetBytes(input);

            var value = await DoIt<string>(x => new StringHandler(x), input);
            value.Should().Be(input);

            var valueBytes = await DoIt<string>(x => new StringHandler(x), inputBytes);
            valueBytes.Should().Be(input);

            var valueAsync = await DoItAsync<string>(x => new StringHandler(x), input);
            valueAsync.Should().Be(input);

            var valueBytesAsync = await DoItAsync<string>(x => new StringHandler(x), inputBytes);
            valueBytesAsync.Should().Be(input);
        }

        [Fact]
        public async void ShouldXml()
        {
            var input = "<xml>content</xml>";
            var inputBytes = Encoding.Default.GetBytes(input);

            var value = await DoIt<XDocument>(x => new XmlHandler(x), input);
            value.ToString().Should().BeEquivalentTo(input);

            var valueBytes = await DoIt<XDocument>(x => new XmlHandler(x), inputBytes);
            valueBytes.ToString().Should().BeEquivalentTo(input);

            var valueAsync = await DoItAsync<XDocument>(x => new XmlHandler(x), input);
            valueAsync.ToString().Should().BeEquivalentTo(input);

            var valueBytesAsync = await DoItAsync<XDocument>(x => new XmlHandler(x), inputBytes);
            valueBytesAsync.ToString().Should().BeEquivalentTo(input);
        }
    }
}