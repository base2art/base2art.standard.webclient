namespace Base2art.WebClient.Features
{
    using System;
    using FluentAssertions;
    using Xunit;

    public class WebClientUriFeatures
    {
        [Fact]
        public void ShouldLoadToString()
        {
            var buildId = new Guid("395459FC-E941-4F61-9434-281A3BE317B4");
            var uri = new WebClientUri("https://something.com/test/")
                      .WithResourcePath("api/v1/builds/{buildId}")
                      .WithUrlSegment("buildId", buildId.ToString())
                      .ToString();

            uri.Should().Be("https://something.com/test/api/v1/builds/395459fc-e941-4f61-9434-281a3be317b4");
        }

        [Fact]
        public void ShouldLoadToUri()
        {
            var buildId = new Guid("395459FC-E941-4F61-9434-281A3BE317B4");
            var uri = new WebClientUri("https://something.com/test")
                      .WithResourcePath("api/v1/builds/{buildId}")
                      .WithUrlSegment("buildId", buildId.ToString())
                      .ToUri();

            uri.Should().Be(new Uri("https://something.com/api/v1/builds/395459fc-e941-4f61-9434-281a3be317b4"));
        }

        [Fact]
        public void ShouldLoadToUriEndingSlash()
        {
            var buildId = new Guid("395459FC-E941-4F61-9434-281A3BE317B4");
            var uri = new WebClientUri("https://something.com/test/")
                      .WithResourcePath("api/v1/builds/{buildId}")
                      .WithUrlSegment("buildId", buildId.ToString())
                      .ToUri();

            uri.Should().Be(new Uri("https://something.com/test/api/v1/builds/395459fc-e941-4f61-9434-281a3be317b4"));
        }

        [Fact]
        public void ShouldNoPath()
        {
            var buildId1 = new Guid("395459FC-E941-4F61-9434-281A3BE317B4");
            var buildId2 = new Guid("395459FC-E941-4F61-9434-281A3BE317B5");
            var uri = new WebClientUri("https://something.com/test")
                      .WithUrlSegment("buildId", buildId1.ToString())
                      .WithUrlSegment("buildId", buildId2.ToString())
                      .ToString();

            uri.Should().Be("https://something.com/test?buildId=395459fc-e941-4f61-9434-281a3be317b4&buildId=395459fc-e941-4f61-9434-281a3be317b5");
        }

        [Fact]
        public void ShouldQueryString()
        {
            var buildId = new Guid("395459FC-E941-4F61-9434-281A3BE317B4");
            var uri = new WebClientUri("https://something.com/test/")
                      .WithResourcePath("api/v1/builds/")
                      .WithUrlSegment("buildId", buildId.ToString())
                      .ToString();

            uri.Should().Be("https://something.com/test/api/v1/builds/?buildId=395459fc-e941-4f61-9434-281a3be317b4");
        }

        [Fact]
        public void ShouldQueryString2()
        {
            var buildId = new Guid("395459FC-E941-4F61-9434-281A3BE317B4");
            var uri = new WebClientUri("https://something.com/test/")
                      .WithResourcePath("api/v1/builds/{buildId}")
                      .WithUrlSegment("buildId", buildId.ToString())
                      .WithUrlSegment("buildId", buildId.ToString())
                      .ToString();

            uri.Should()
               .Be("https://something.com/test/api/v1/builds/395459fc-e941-4f61-9434-281a3be317b4?buildId=395459fc-e941-4f61-9434-281a3be317b4");
        }
    }
}