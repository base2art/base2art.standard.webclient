namespace Base2art.WebClient.Features
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Fixtures;
    using FluentAssertions;
    using Json;
    using NetHttp;
    using NetRequests;
    using Newtonsoft.Json;
    using ResponseHandlers;
    using ResponseHandlers.Html;
    using ResponseHandlers.Json;
    using Xunit;

    public class UnitTest1
    {
        [Theory]
        [InlineData(typeof(SimpleWebClient))]
        [InlineData(typeof(SimpleHttpWebClient))]
        public async void ShouldGet(Type type)
        {
            var actual = "";
            await ((IWebClient) Activator.CreateInstance(type))
                  .CreateRequest("http://scottyoungblut.com/")
                  .WithResponseHandler()
                  .WithStatusHandler(HttpStatusCode.OK, new HtmlHandler(x =>
                  {
                      actual = x.DocumentNode
                                .SelectSingleNode("//*[@class='skillset']/div[6]")
                                .InnerText
                                .Trim();
                  }))
                  .Run();

            actual.Should().Be("JWT");
        }

        [Theory]
        [InlineData(typeof(SimpleWebClient))]
        [InlineData(typeof(SimpleHttpWebClient))]
        public async void ShouldPostString(Type type)
        {
            var actual = "";
            await ((IWebClient) Activator.CreateInstance(type))
                  .CreateRequest("https://www.w3schools.com/action_page.php")
                  .WithMethod(HttpMethod.Post)
                  .WithContentType("application/x-www-form-urlencoded")
                  .WithTextBody(x => x.Write("fname=test&lname=User"))
                  .WithResponseHandler()
                  .WithStatusHandler(HttpStatusCode.OK, new StringHandler(x => { actual = x; }))
                  .Run();

            actual.Should().Contain("fname=test&lname=User");
        }

        [Theory]
        [InlineData(typeof(SimpleWebClient))]
        [InlineData(typeof(SimpleHttpWebClient))]
        public async void ShouldPostBytes(Type type)
        {
            var actual = "";
            await ((IWebClient) Activator.CreateInstance(type))
                  .CreateRequest("https://www.w3schools.com/action_page.php")
                  .WithMethod(HttpMethod.Post)
                  .WithContentType(KnownContentTypes.wwwFormUrlEncoded)
                  .WithBinaryBody(x => x.Write(Encoding.Default.GetBytes("fname=test&lname=User")))
                  .WithResponseHandler()
                  .WithStatusHandler(HttpStatusCode.OK, new StringHandler(x => { actual = x; }))
                  .Run();

            actual.Should().Contain("fname=test&lname=User");
        }

        [Theory]
        [InlineData(typeof(SimpleWebClient))]
        [InlineData(typeof(SimpleHttpWebClient))]
        public async void ShouldRunJsonBody(Type type)
        {
            dynamic actual = null;
            await ((IWebClient) Activator.CreateInstance(type))
                  .CreateRequest("https://httpbin.org/put")
                  .WithMethod(HttpMethod.Put)
                  .WithJsonBody(new {a = 123})
                  .WithResponseHandler()
                  .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<dynamic>(x => { actual = x; }))
                  .Run();

            long i = actual.json.a;
            i.Should().Be(123);
        }

        [Theory]
        [InlineData(typeof(SimpleWebClient), typeof(WebException))]
        [InlineData(typeof(SimpleHttpWebClient), typeof(TaskCanceledException))]
        public async void ShouldLowTimeout(Type type, Type exceptionType)
        {
            dynamic actual = null;
            var item = ((IWebClient) Activator.CreateInstance(type))
                       .CreateRequest("https://httpbin.org/put")
                       .WithTimeout(TimeSpan.FromMilliseconds(2))
                       .WithMethod(HttpMethod.Put)
                       .WithJsonBody(new {a = 123})
                       .WithResponseHandler()
                       .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<dynamic>(x => { actual = x; }));

            try
            {
                await item.Run();
            }
            catch (Exception e)
            {
                e.GetType().Should().Be(exceptionType);
                // if (e.GetType() == exceptionType)
                // {
                //     return;
                // }
            }

            // Assert.True(false, "Wrong type.");
        }

        private class KnownContentTypes
        {
            public static string wwwFormUrlEncoded => "application/x-www-form-urlencoded";
        }

        [Fact]
        public async void Test1()
        {
            var data =
                @"{""buildId"":""9547a811-2b04-41c4-8de7-8f5228b58554"",""state"":""Pending"",""completedAt"":null,""output"":"""",""error"":""""}";
            ProjectBuild pb = null;
            var handler = new JsonHandler<ProjectBuild>(build => pb = build);

            await handler.Invoke(data);

            pb.State.Should().Be(BuildState.Pending);
        }

        [Theory(Skip = "Ignored by Sjy")]
        [InlineData(typeof(SimpleWebClient))]
        [InlineData(typeof(SimpleHttpWebClient))]
        public async void ShouldRunJsonBodyRealWorldBigBody(Type type)
        {
            var url = "https://publishserver-01.dwtapps.com/api/v1/artifacts";
            var key = "eyJhbGciOiJSUzUxMiIsImtpZCI6InZlcnNpb24xIiwidHlwIjoiSldUIn0.eyJ1bmlxdWVfbmFtZSI6InNrZWxhdG9yIiwiaHR0cDovL3NjaGVtYXM"
                      + "ubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy92ZXJzaW9uIjoiMSIsIm5iZiI6MTYwNzQwMDE0NywiZXhwIjoxNjA3NDE4NzQ3LC"
                      + "JpYXQiOjE2MDc0MDA3NDcsImlzcyI6Imh0dHA6Ly9idWlsZC1tYXN0ZXIuYmFzZTJhcnQuY29tLyIsImF1ZCI6Imh0dHA6Ly9wdWJsaXNoLW5vZGUuYmF"
                      + "zZTJhcnQuY29tLyJ9.jvOQQfD8H_M0gtwCZR9d110eA3TwcgX3NCWt9UE3T99LBrTrLnHyj7Y2wY8j8G7VJ36RTk_-"
                      + "9RoZyGA6Y81ua5mJZbeKVPpw87Nt7Km2jyPCeoiOSUqrEUBZM0MieLBwSUe-rcsXOxppYOlJ4NO-G7P2PVPsRe5sR_GHPUbDtFcHYD_JV4tZ3UzIwwBkGA"
                      + "fHqjuSy8ixloO0M5DMK7kdnpZp2dCt5H9nyOH5-Xe57emFsfnmpLuKYq6PKJHTI4pbdtiMCvhYsryNv_stTz-6LyQWTuRKJwupoO7gm4VankxYl6pW4z37_"
                      + "6j5nBU8Ite4JPG_KL2YwXAlPy64Zhn81Q";

            var content = await File.ReadAllTextAsync("/home/tyoung/code/b2a/cicd-coordinator/src/Base2art.CiCd.Coordinator.Web/publish.debug.json");

            // var message = new HttpRequestMessage(HttpMethod.Put, url);
            // client.Timeout = TimeSpan.FromMinutes(50);
            // message.Headers.Authorization = new AuthenticationHeaderValue($"Bearer", key);
            // message.Content = new StringContent(content, Encoding.Default, "application/json");

            // await client.SendAsync(message, HttpCompletionOption.ResponseHeadersRead);
            var data = JsonConvert.DeserializeObject<PublishEvent>(content);
            ProjectPublish actual = null;
            var webClient = ((IWebClient) Activator.CreateInstance(type));

            await webClient.CreateRequest(url)
                           .WithTimeout(TimeSpan.FromMinutes(15))
                           .WithMethod(HttpMethod.Put)
                           .WithHeader("Authorization", "Bearer " + key)
                           .WithJsonBody(data)
                           .WithResponseHandler()
                           .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<ProjectPublish>(x => { actual = x; }))
                           .Run();

            actual.Id.Should().NotBeEmpty();
        }

        [Theory(Skip = "Ignored by Sjy")]
        [InlineData(typeof(SimpleWebClient))]
        [InlineData(typeof(SimpleHttpWebClient))]
        public async void ShouldRunJsonBodyRealWorldAuthHeader(Type type)
        {
            var url = "https://publishserver-01.dwtapps.com/api/v1/supported-features";
            var key = "eyJhbGciOiJSUzUxMiIsImtpZCI6InZlcnNpb24xIiwidHlwIjoiSldUIn0.eyJ1bmlxdWVfbmFtZSI6InNrZWxhdG9yIiwiaHR0cDovL3NjaGVtYXM"
                      + "ubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy92ZXJzaW9uIjoiMSIsIm5iZiI6MTYwNzQwMDE0NywiZXhwIjoxNjA3NDE4NzQ3LC"
                      + "JpYXQiOjE2MDc0MDA3NDcsImlzcyI6Imh0dHA6Ly9idWlsZC1tYXN0ZXIuYmFzZTJhcnQuY29tLyIsImF1ZCI6Imh0dHA6Ly9wdWJsaXNoLW5vZGUuYmF"
                      + "zZTJhcnQuY29tLyJ9.jvOQQfD8H_M0gtwCZR9d110eA3TwcgX3NCWt9UE3T99LBrTrLnHyj7Y2wY8j8G7VJ36RTk_-"
                      + "9RoZyGA6Y81ua5mJZbeKVPpw87Nt7Km2jyPCeoiOSUqrEUBZM0MieLBwSUe-rcsXOxppYOlJ4NO-G7P2PVPsRe5sR_GHPUbDtFcHYD_JV4tZ3UzIwwBkGA"
                      + "fHqjuSy8ixloO0M5DMK7kdnpZp2dCt5H9nyOH5-Xe57emFsfnmpLuKYq6PKJHTI4pbdtiMCvhYsryNv_stTz-6LyQWTuRKJwupoO7gm4VankxYl6pW4z37_"
                      + "6j5nBU8Ite4JPG_KL2YwXAlPy64Zhn81Q";

            List<SupportedFeature> actual = null;
            var webClient = ((IWebClient) Activator.CreateInstance(type));

            await webClient.CreateRequest(url)
                           .WithTimeout(TimeSpan.FromMinutes(15))
                           .WithMethod(HttpMethod.Get)
                           .WithHeader("Authorization", "Bearer " + key)
                           // .WithJsonBody(data)
                           .WithResponseHandler()
                           .WithStatusHandler(HttpStatusCode.OK, new JsonHandler<List<SupportedFeature>>(x => { actual = x; }))
                           .Run();

            actual.Count.Should().Be(3);
        }
    }

    public class SupportedFeature
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public Dictionary<string, object> DefaultData { get; set; }
    }
}