namespace Base2art.WebClient.Features.Fixtures
{
    using System;

    public class ProjectBuild
    {
        public Guid BuildId { get; set; }

        public BuildState State { get; set; }

        public DateTime? CompletedAt { get; set; }

        public string Output { get; set; }

        public string Error { get; set; }
    }

    public enum BuildState
    {
        Unknown,
        Pending,
        Claimed,
        Working,
        CompletedSuccess,
        CompletedFail
    }

    public class KnownContentTypes
    {
        public static string wwwFormUrlEncoded => "application/x-www-form-urlencoded";
    }

    public class PublishEvent
    {
        public Guid JobId { get; set; }

        public Project Project { get; set; }
        public BranchData[] Branches { get; set; }
        public FileData[] Artifacts { get; set; }
    }

    public class Project
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class BranchData
    {
        public string Id { get; set; }
        public Guid TypeId { get; set; }
        public string Name { get; set; }
        public string Hash { get; set; }

        public override string ToString() => $"{nameof(this.Name)}: {this.Name}, {nameof(this.Hash)}: {this.Hash}";
    }

    public class FileData
    {
        public string Name { get; set; }
        public string RelativeName { get; set; }
        public byte[] Content { get; set; }
    }

    public class ProjectPublish
    {
        public Guid Id { get; set; }
        public PublishPhaseState State { get; set; }
        public ProjectHandlerOutput[] Output { get; set; }
        public DateTime? CompletedAt { get; set; }
    }

    public enum PublishPhaseState
    {
        Unknown = 0,
        Pending = 1,
        Claimed = 2,
        Working = 3,
        CompletedSuccess = 4,
        CompletedFail = 5
    }

    public class ProjectHandlerOutput
    {
        public Guid HandlerId { get; set; }
        public string HandlerName { get; set; }
        public string Output { get; set; }
        public string Error { get; set; }
    }
}