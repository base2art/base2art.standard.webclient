echo "cleaning local packages..."

echo "cleaning local packages..."
$projs = Get-ChildItem -Path "src"
foreach ($item in $projs)
{
    $binPath = "$( $item.FullName )/bin/"
    if (Test-Path $binPath)
    {
        Remove-Item -Recurse -Force $binPath
    }
    $objPath = "$( $item.FullName )/obj/"
    if (Test-Path $objPath)
    {
        Remove-Item -Recurse -Force $objPath
    }
}

echo "cleaning..."
dotnet clean --configuration Release -v q

echo "building..."
dotnet build --configuration Release -v q
dotnet build --configuration Release -v q



echo "testing..."
$projs = Get-ChildItem -Path "test"
foreach ($item in $projs)
{
    $csprojs = Get-ChildItem -Path $item.FullName -Filter "*.csproj"

    foreach ($csproj in $csprojs)
    {
        #-v q 
        dotnet test --configuration Release "$( $csproj.FullName )"

        if (-not$?)
        {
            exit $LASTEXITCODE
        }
    }
}

echo "packing..."
dotnet pack --include-symbols --include-source  -v q --configuration Release "/p:PackageVersion=0.0.0.15"

if (-Not(Test-Path "output-packages"))
{
    mkdir "output-packages"
}

echo "pushing to local..."

$projs = Get-ChildItem -Path "src"
foreach ($item in $projs)
{if ($item.Name -ne "common")
    {
        cp "$( $item.FullName )/bin/Release/*.nupkg" "/data/nuget-repository/"
        cp "$( $item.FullName )/bin/Release/*.nupkg" ./output-packages/
        rm ./output-packages/*.symbols.nupkg
    }
}

