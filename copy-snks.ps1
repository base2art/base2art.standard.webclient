
$srcItems = Get-ChildItem -path "src" | ?{ $_.PSIsContainer } 
$testItems = Get-ChildItem -path "test" | ?{ $_.PSIsContainer } 

function copyTo($item) {
  
  Copy-Item -Path ~/.nuget/packages/base2art.keys/1.0.0/content/base2art.snk -Destination $item.FullName 
}

foreach ($item in $srcItems) {
  copyTo $item
}

foreach ($item in $testItems) {
  copyTo $item
}

