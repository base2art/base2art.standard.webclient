﻿namespace Base2art.WebClient.ResponseHandlers.Json
{
    using System;
    using System.Threading.Tasks;
    using WebClient.Json.Serialization;

    public class JsonHandler<T> : TextHandler
    {
        private readonly Func<T, Task> func;

        public JsonHandler(Action<T> act) : this(FuncItAsync(act))
        {
        }

        public JsonHandler(Func<T, Task> func) => this.func = func;

        public override Task Invoke(string value) => this.func(new CamelCasingSimpleJsonSerializer().Deserialize<T>(value));

        public static Func<T1, Task> FuncItAsync<T1>(Action<T1> action)
        {
            return x =>
            {
                action(x);
                return Task.CompletedTask;
            };
        }
    }
}