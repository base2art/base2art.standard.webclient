namespace Base2art.WebClient.Json
{
    using System;
    using Serialization;

    public static class WebClientRequestBuilders
    {
        private static readonly IJsonSerializer Serializer = new CamelCasingSimpleJsonSerializer();

        public static IWebClientRequestBuilder WithJsonBody<T>(this IWebClientRequestBuilder requestBuilder, T obj)
        {
            if (requestBuilder == null)
            {
                throw new ArgumentNullException(nameof(requestBuilder));
            }

            return requestBuilder.WithContentType("application/json")
                                 .WithTextBody(x =>
                                 {
                                     var content = Serializer.Serialize(obj);
                                     x.Write(content);
                                     x.Flush();
                                 });
        }
    }
}