﻿namespace Base2art.WebClient.NetRequests
{
    using System;
    using System.Net;
    using Common;

    public class SimpleWebClient : WebClientBase<HttpWebResponse>
    {
        protected override WebClientRequestBuilderBase<HttpWebResponse> CreateRequest(string url, TimeSpan? defaultTimeout)
            => new WebClientRequestBuilder(url, this.Container, defaultTimeout);
    }
}