﻿namespace Base2art.WebClient.NetRequests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Common;

    public class WebClientResponseBuilder : WebClientResponseBuilderBase<HttpWebResponse>
    {
        private readonly HttpWebRequest request;

        public WebClientResponseBuilder(
            string url,
            IReadOnlyDictionary<string, string> headers,
            CookieContainer cookieContainer,
            HttpMethod method,
            string contentType,
            TimeSpan? timeout,
            Action<TextWriter> textWriter,
            Action<IByteWriter> binaryWriter) : base(textWriter, binaryWriter)
        {
            this.request = WebRequest.CreateHttp(url);

            foreach (var pair in headers ?? new Dictionary<string, string>())
            {
                this.request.Headers.Add(pair.Key, pair.Value);
            }
            
            this.request.CookieContainer = cookieContainer;
            this.request.Method = method.Method;

            if (timeout.HasValue)
            {
                this.request.Timeout = (int) timeout.Value.TotalMilliseconds;
            }

            if (!string.IsNullOrWhiteSpace(contentType))
            {
                this.request.ContentType = contentType;
            }
        }

        protected override async Task SetRequestBytes(byte[] buffer)
        {
            using (var stream = await this.request.GetRequestStreamAsync())
            {
                stream.Write(buffer, 0, buffer.Length);
            }
        }

        protected override void SetContentLength(int bufferLength)
        {
            this.request.Headers[HttpRequestHeader.ContentLength] = bufferLength.ToString();
        }

        protected override async Task<HttpWebResponse> CreateResponse() => (HttpWebResponse) await this.request.GetResponseAsync();

        protected override Task<Stream> GetResponseStreamAsync(HttpWebResponse response) => Task.FromResult(response.GetResponseStream());

        protected override string GetContentType(HttpWebResponse response) => response.ContentType;

        protected override HttpStatusCode GetStatusCode(HttpWebResponse response) => response.StatusCode;
    }
}
