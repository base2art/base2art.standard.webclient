﻿namespace Base2art.WebClient.NetRequests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using Common;

    public class WebClientRequestBuilder : WebClientRequestBuilderBase<HttpWebResponse>
    {
        private readonly CookieContainer cookieContainer;

        public WebClientRequestBuilder(string url, CookieContainer cookieContainer, TimeSpan? defaultTimeout) : base(url, defaultTimeout)
            => this.cookieContainer = cookieContainer;

        protected override WebClientResponseBuilderBase<HttpWebResponse> CreateResponseHandler(
            string url,
            IReadOnlyDictionary<string, string> headers,
            HttpMethod method,
            string contentType,
            TimeSpan? timeout,
            Action<TextWriter> textWriter,
            Action<IByteWriter> binaryWriter) => new WebClientResponseBuilder(
                                                                              url,
                                                                              headers,
                                                                              this.cookieContainer,
                                                                              method,
                                                                              contentType,
                                                                              timeout,
                                                                              textWriter,
                                                                              binaryWriter);
    }
}