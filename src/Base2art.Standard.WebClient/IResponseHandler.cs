﻿namespace Base2art.WebClient
{
    using System.Threading.Tasks;

    public interface IResponseHandler
    {
        Task Invoke(string value);
        Task Invoke(byte[] value);
    }
}