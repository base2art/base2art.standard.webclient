namespace Base2art.WebClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class WebClientUri
    {
        private readonly string baseUrl;
        private readonly Dictionary<string, List<string>> urlParams = new Dictionary<string, List<string>>();
        private string path;

        public WebClientUri(string baseUrl) => this.baseUrl = baseUrl;

        public WebClientUri WithResourcePath(string path)
        {
            this.path = path;
            return this;
        }

        public WebClientUri WithUrlSegment(string parameterName, string parameterValue)
        {
            if (!this.urlParams.ContainsKey(parameterName))
            {
                this.urlParams[parameterName] = new List<string>();
            }

            this.urlParams[parameterName].Add(parameterValue);

            return this;
        }

        public Uri ToUri()
        {
            var uri = new Uri(this.baseUrl);
            var localPath = this.path;
            var qsItems = new Dictionary<string, List<string>>();

            if (!string.IsNullOrWhiteSpace(localPath))
            {
                foreach (var param in this.urlParams.Keys)
                {
                    var items = this.urlParams[param];

                    foreach (var item in items)
                    {
                        localPath = this.Augment(localPath, param, item, qsItems);
                    }
                }
            }
            else
            {
                qsItems = this.urlParams;
            }

            string concat(Tuple<string, string> item) => string.Concat(Uri.EscapeUriString(item.Item1 ?? ""),
                                                                       "=",
                                                                       Uri.EscapeUriString(item.Item2 ?? ""));

            var queryString = string.Join("&", qsItems.SelectMany(x => x.Value.Select(y => Tuple.Create(x.Key, y))).Select(concat));

            var uriBuilder = new UriBuilder(new Uri(uri, localPath)) {Query = queryString};

            return uriBuilder.Uri;
        }

        private string Augment(string localPath, string param, string value, Dictionary<string, List<string>> qsItems)
        {
            var key = "{" + param + "}";
            if (localPath.Contains(key))
            {
                return localPath.Replace(key, Uri.EscapeUriString(value));
            }

            if (!qsItems.ContainsKey(param))
            {
                qsItems[param] = new List<string>();
            }

            qsItems[param].Add(value);
            return localPath;
        }

        public override string ToString() => this.ToUri().ToString();
    }
}