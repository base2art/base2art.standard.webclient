﻿namespace Base2art.WebClient
{
    using System;

    public interface IWebClient : IDisposable
    {
        TimeSpan DefaultTimeout { get; set; }

        IWebClientRequestBuilder CreateRequest(string url);
    }
}