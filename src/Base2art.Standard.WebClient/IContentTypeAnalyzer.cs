﻿namespace Base2art.WebClient
{
    public interface IContentTypeAnalyzer
    {
        bool IsString(string contentType);
    }
}