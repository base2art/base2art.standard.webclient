﻿namespace Base2art.WebClient
{
    public interface IByteWriter
    {
        void Write(byte[] bytes);
    }
}