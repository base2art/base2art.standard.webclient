﻿namespace Base2art.WebClient.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Internals;

    public abstract class WebClientResponseBuilderBase<TResponse> : IWebClientResponseBuilder
    {
        private readonly Action<IByteWriter> binaryWriter;

        private readonly Dictionary<HttpStatusCode, IResponseHandler> statusLookup = new Dictionary<HttpStatusCode, IResponseHandler>();
        private readonly Action<TextWriter> textWriter;
        private IContentTypeAnalyzer analyzer;

        protected WebClientResponseBuilderBase(
            Action<TextWriter> textWriter,
            Action<IByteWriter> binaryWriter)
        {
            this.textWriter = textWriter;
            this.binaryWriter = binaryWriter;
        }

        public IWebClientResponseBuilder WithContentTypeAnalyzer(IContentTypeAnalyzer analyzer)
        {
            this.analyzer = analyzer;
            return this;
        }

        public IWebClientResponseBuilder WithStatusHandler(HttpStatusCode code, IResponseHandler func)
        {
            this.statusLookup[code] = func;
            return this;
        }

        public async Task Run()
        {
            if (this.binaryWriter != null)
            {
                var byteWriter = new ByteWriter();
                this.binaryWriter(byteWriter);
                var buffer = byteWriter.ToArray();
                this.SetContentLength(buffer.Length);
                await this.SetRequestBytes(buffer);
            }

            if (this.textWriter != null)
            {
                string result;
                using (var writer = new StringWriter())
                {
                    this.textWriter(writer);
                    await writer.FlushAsync();
                    result = writer.GetStringBuilder().ToString();
                }

                var buffer = Encoding.UTF8.GetBytes(result);
                this.SetContentLength(buffer.Length);
                await this.SetRequestBytes(buffer);
            }

            var response = await this.CreateResponse();
            var statusCode = this.GetStatusCode(response);
            var (isString, str, bytes) = await this.GetTypeAndValue(response);
            if (this.statusLookup.ContainsKey(statusCode))
            {
                var handler = this.statusLookup[statusCode];
                await (isString ? handler.Invoke(str) : handler.Invoke(bytes));
            }
        }

        protected abstract Task<TResponse> CreateResponse();

        protected abstract Task SetRequestBytes(byte[] buffer);

        protected abstract void SetContentLength(int bufferLength);

        protected abstract Task<Stream> GetResponseStreamAsync(TResponse response);

        protected abstract string GetContentType(TResponse response);

        protected abstract HttpStatusCode GetStatusCode(TResponse response);

        private async Task<Tuple<bool, string, byte[]>> GetTypeAndValue(TResponse response)
        {
            var isString = (this.analyzer ?? new StandardContentTypeAnalyzer()).IsString(this.GetContentType(response));

            using (var stream = await this.GetResponseStreamAsync(response))
            {
                if (!isString)
                {
                    return Tuple.Create<bool, string, byte[]>(false, null, stream.ReadFully());
                }

                using (var sr = new StreamReader(stream))
                {
                    return Tuple.Create<bool, string, byte[]>(true, sr.ReadToEnd(), null);
                }
            }
        }
    }
}