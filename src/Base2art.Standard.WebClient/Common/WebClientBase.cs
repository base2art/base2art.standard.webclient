﻿namespace Base2art.WebClient.Common
{
    using System;
    using System.Net;

    public abstract class WebClientBase<TResponse> : IWebClient
    {
        protected CookieContainer Container { get; } = new CookieContainer();

        IWebClientRequestBuilder IWebClient.CreateRequest(string url)
            => this.CreateRequest(
                                  url,
                                  this.DefaultTimeout <= TimeSpan.Zero
                                      ? (TimeSpan?) null
                                      : this.DefaultTimeout);

        public TimeSpan DefaultTimeout { get; set; }

        // Dispose() calls Dispose(true)  
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~WebClientBase()
        {
            // Finalizer calls Dispose(false)  
            this.Dispose(false);
        }

        protected abstract WebClientRequestBuilderBase<TResponse> CreateRequest(string url, TimeSpan? defaultTimeout);

        // The bulk of the clean-up code is implemented in Dispose(bool)  
        protected virtual void Dispose(bool disposing)
        {
        }
    }
}