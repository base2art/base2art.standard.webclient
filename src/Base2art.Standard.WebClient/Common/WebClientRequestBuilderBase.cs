﻿namespace Base2art.WebClient.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;

    public abstract class WebClientRequestBuilderBase<TResponse> : IWebClientRequestBuilder
    {
        private readonly TimeSpan? defaultTimeout;
        private readonly Dictionary<string, string> headers = new Dictionary<string, string>();
        private readonly string url;
        private Action<IByteWriter> binaryWriter;
        private string contentType;
        private TimeSpan? delay;

        private HttpMethod method;

        private Action<TextWriter> textWriter;

        protected WebClientRequestBuilderBase(string url, TimeSpan? defaultTimeout)
        {
            this.url = url;
            this.defaultTimeout = defaultTimeout;
        }

        public IWebClientRequestBuilder WithMethod(HttpMethod value)
        {
            this.method = value;
            return this;
        }

        public IWebClientRequestBuilder WithTimeout(TimeSpan delay)
        {
            this.delay = delay;
            return this;
        }

        public IWebClientRequestBuilder WithTextBody(Action<TextWriter> func)
        {
            this.textWriter = func;
            this.binaryWriter = null;
            return this;
        }

        public IWebClientRequestBuilder WithBinaryBody(Action<IByteWriter> func)
        {
            this.binaryWriter = func;
            this.textWriter = null;
            return this;
        }

        public IWebClientRequestBuilder WithContentType(string contentType)
        {
            this.contentType = contentType;
            return this;
        }

        public IWebClientRequestBuilder WithHeader(string headerName, string headerValue)
        {
            if (!string.IsNullOrWhiteSpace(headerName))
            {
                this.headers[headerName] = headerValue ?? string.Empty;
            }

            return this;
        }

        public IWebClientResponseBuilder WithResponseHandler() => this.CreateResponseHandler(
                                                                                             this.url,
                                                                                             this.headers,
                                                                                             this.method ?? HttpMethod.Get,
                                                                                             this.contentType,
                                                                                             this.GetTimeout(),
                                                                                             this.textWriter,
                                                                                             this.binaryWriter);

        private TimeSpan? GetTimeout()
            => this.delay.HasValue ? this.delay.Value : this.defaultTimeout;

        protected abstract WebClientResponseBuilderBase<TResponse> CreateResponseHandler(
            string url,
            IReadOnlyDictionary<string, string> headers,
            HttpMethod method,
            string contentType,
            TimeSpan? timeout,
            Action<TextWriter> textWriter,
            Action<IByteWriter> binaryWriter);
    }
}