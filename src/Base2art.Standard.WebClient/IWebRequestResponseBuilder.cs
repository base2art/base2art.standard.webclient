﻿namespace Base2art.WebClient
{
    using System.Net;
    using System.Threading.Tasks;

    public interface IWebClientResponseBuilder
    {
        IWebClientResponseBuilder WithStatusHandler(HttpStatusCode code, IResponseHandler func);

        IWebClientResponseBuilder WithContentTypeAnalyzer(IContentTypeAnalyzer analyzer);

        Task Run();
    }
}