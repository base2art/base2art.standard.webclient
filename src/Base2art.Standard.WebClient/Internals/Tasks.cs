﻿namespace Base2art.WebClient.Internals
{
    using System;
    using System.Threading.Tasks;

    internal static class Tasks
    {
        public static Func<T, Task> FuncItAsync<T>(this Action<T> action)
        {
            return x =>
            {
                action(x);
                return Task.CompletedTask;
            };
        }
    }
}