﻿namespace Base2art.WebClient.Internals
{
    using System;
    using System.IO;

    internal class ByteWriter : IByteWriter
    {
        private readonly MemoryStream stream = new MemoryStream();

        public void Write(byte[] bytes)
        {
            if (bytes == null)
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            this.Write(bytes, 0, bytes.Length);
        }

        public void Write(byte[] bytes, int offset, int count)
        {
            if (bytes == null)
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            this.stream.Write(bytes, offset, count);
        }

        public byte[] ToArray() => this.stream.ToArray();
    }
}