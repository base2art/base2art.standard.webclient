﻿namespace Base2art.WebClient.Internals
{
    using System;
    using System.Collections.Generic;

    internal class StandardContentTypeAnalyzer : IContentTypeAnalyzer
    {
        private readonly HashSet<string> applicationContentTypes = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
                                                                   {
                                                                       "application/xml",
                                                                       "application/typescript",
                                                                       "application/json",
                                                                       "application/javascript",
                                                                       "application/x-csh"
                                                                   };

        public bool IsString(string contentType)
        {
            if (string.IsNullOrWhiteSpace(contentType))
            {
                return false;
            }

            if (contentType.StartsWith("text/", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            if (contentType.StartsWith("application/", StringComparison.OrdinalIgnoreCase))
            {
                if (this.applicationContentTypes.Contains(contentType))
                {
                    return true;
                }
            }

            if (contentType.EndsWith("+xml"))
            {
                return true;
            }

            return false;
        }
    }
}