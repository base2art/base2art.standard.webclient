﻿namespace Base2art.WebClient
{
    using System;
    using System.IO;
    using System.Net.Http;

    public interface IWebClientRequestBuilder
    {
        IWebClientRequestBuilder WithMethod(HttpMethod get);

        IWebClientRequestBuilder WithTextBody(Action<TextWriter> func);

        IWebClientRequestBuilder WithBinaryBody(Action<IByteWriter> func);

        IWebClientRequestBuilder WithContentType(string contentType);

        IWebClientRequestBuilder WithHeader(string headerName, string headerValue);

        IWebClientRequestBuilder WithTimeout(TimeSpan delay);

        IWebClientResponseBuilder WithResponseHandler();
    }
}