namespace Base2art.WebClient.ResponseHandlers
{
    using System.Text;
    using System.Threading.Tasks;

    public abstract class TextHandler : IResponseHandler
    {
        public abstract Task Invoke(string value);

        public Task Invoke(byte[] value) => this.Invoke(Encoding.UTF8.GetString(value));
    }
}