namespace Base2art.WebClient.ResponseHandlers
{
    using System.Text;
    using System.Threading.Tasks;

    public abstract class BinaryHandler : IResponseHandler
    {
        public Task Invoke(string value) => this.Invoke(Encoding.UTF8.GetBytes(value));

        public abstract Task Invoke(byte[] value);
    }
}