namespace Base2art.WebClient.ResponseHandlers
{
    using System;
    using System.Threading.Tasks;
    using Internals;

    public class StringHandler : TextHandler
    {
        private readonly Func<string, Task> func;

        public StringHandler(Action<string> act) : this(act.FuncItAsync())
        {
        }

        public StringHandler(Func<string, Task> func) => this.func = func;

        public override Task Invoke(string value) => this.func(value);
    }
}