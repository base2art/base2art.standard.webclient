﻿namespace Base2art.WebClient.ResponseHandlers
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using Internals;

    public class XmlHandler : IResponseHandler
    {
        private readonly Func<XDocument, Task> func;

        public XmlHandler(Action<XDocument> act) : this(act.FuncItAsync())
        {
        }

        public XmlHandler(Func<XDocument, Task> func) => this.func = func;

        public Task Invoke(string value)
        {
            var value1 = XDocument.Parse(value);
            return this.func(value1);
        }

        public Task Invoke(byte[] value)
        {
            using (var memoryStream = new MemoryStream(value))
            {
                memoryStream.Seek(0L, SeekOrigin.Begin);
                var value1 = XDocument.Load(memoryStream);
                return this.func(value1);
            }
        }
    }
}