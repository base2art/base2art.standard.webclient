namespace Base2art.WebClient.ResponseHandlers
{
    using System;
    using System.Threading.Tasks;
    using Internals;

    public class ByteArrayHandler : BinaryHandler
    {
        private readonly Func<byte[], Task> func;

        public ByteArrayHandler(Action<byte[]> act) : this(act.FuncItAsync())
        {
        }

        public ByteArrayHandler(Func<byte[], Task> func) => this.func = func;

        public override Task Invoke(byte[] value) => this.func(value);
    }
}