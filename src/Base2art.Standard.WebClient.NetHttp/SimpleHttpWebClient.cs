﻿namespace Base2art.WebClient.NetHttp
{
    using System;
    using System.Net.Http;
    using Common;

    public class SimpleHttpWebClient : WebClientBase<HttpResponseMessage>
    {
        private readonly HttpClient client;
        private readonly HttpClientHandler clientHandler;

        public SimpleHttpWebClient()
        {
            this.clientHandler = new HttpClientHandler {CookieContainer = this.Container};
            this.client = new HttpClient(this.clientHandler);
        }

        protected override WebClientRequestBuilderBase<HttpResponseMessage> CreateRequest(string url, TimeSpan? defaultTimeout)
            => new WebClientRequestBuilder(url, this.client, defaultTimeout);
    }
}