﻿namespace Base2art.WebClient.NetHttp
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using Common;

    public class WebClientRequestBuilder : WebClientRequestBuilderBase<HttpResponseMessage>
    {
        private readonly HttpClient client;

        public WebClientRequestBuilder(string url, HttpClient client, TimeSpan? defaultTimeout) : base(url, defaultTimeout)
            => this.client = client;

        protected override WebClientResponseBuilderBase<HttpResponseMessage> CreateResponseHandler(
            string url,
            IReadOnlyDictionary<string, string> headers,
            HttpMethod method,
            string contentType,
            TimeSpan? timeout,
            Action<TextWriter> textWriter,
            Action<IByteWriter> binaryWriter) =>
            new WebClientResponseBuilder(this.client, url, headers, method, contentType, timeout, textWriter, binaryWriter);
    }
}