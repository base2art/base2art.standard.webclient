﻿namespace Base2art.WebClient.NetHttp
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading;
    using System.Threading.Tasks;
    using Common;

    public class WebClientResponseBuilder : WebClientResponseBuilderBase<HttpResponseMessage>
    {
        private readonly HttpClient client;
        private readonly string contentType;
        private readonly IReadOnlyDictionary<string, string> headers;
        private readonly HttpRequestMessage message;
        private readonly TimeSpan? timeout;

        public WebClientResponseBuilder(
            HttpClient client,
            string url,
            IReadOnlyDictionary<string, string> headers,
            HttpMethod method,
            string contentType,
            TimeSpan? timeout,
            Action<TextWriter> textWriter,
            Action<IByteWriter> binaryWriter) : base(textWriter, binaryWriter)
        {
            this.client = client;
            this.headers = headers;
            this.timeout = timeout;
            this.message = new HttpRequestMessage();
            this.message.RequestUri = new Uri(url);
            this.message.Method = method;

            if (!string.IsNullOrWhiteSpace(contentType))
            {
                this.contentType = contentType;
            }

            var httpRequestHeaders = this.message.Headers;
            foreach (var header in headers)
            {
                if (headers.ContainsKey(header.Key))
                {
                    httpRequestHeaders.Remove(header.Key);
                }

                httpRequestHeaders.Add(header.Key, header.Value);
            }
        }

        protected override Task SetRequestBytes(byte[] buffer)
        {
            this.message.Content = new ByteArrayContent(buffer);
            this.message.Content.Headers.ContentType = new MediaTypeHeaderValue(this.contentType);
            this.message.Content.Headers.ContentLength = buffer.Length;
            return Task.CompletedTask;
        }

        protected override void SetContentLength(int bufferLength)
        {
        }

        protected override Task<HttpResponseMessage> CreateResponse()
        {
            if (!this.timeout.HasValue)
            {
                return this.client.SendAsync(this.message);
            }

            if (this.client.Timeout < this.timeout.Value)
            {
                this.client.Timeout = this.timeout.Value;
            }

            var cts = new CancellationTokenSource();
            cts.CancelAfter(this.timeout.Value);
            return this.client.SendAsync(this.message, cts.Token);
        }

        protected override Task<Stream> GetResponseStreamAsync(HttpResponseMessage response) => response.Content.ReadAsStreamAsync();

        protected override string GetContentType(HttpResponseMessage response) => response?.Content?.Headers?.ContentType?.MediaType ?? string.Empty;

        protected override HttpStatusCode GetStatusCode(HttpResponseMessage response) => response.StatusCode;
    }
}
