﻿// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

using System;
using System.Reflection;

[assembly: AssemblyCompany("Base2art")]
[assembly: AssemblyTrademark("Base2art (c) 2018")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyCulture("")]

[assembly: CLSCompliant(true)]