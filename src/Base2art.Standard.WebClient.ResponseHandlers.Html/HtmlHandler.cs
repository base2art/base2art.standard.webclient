﻿namespace Base2art.WebClient.ResponseHandlers.Html
{
    using System;
    using System.Threading.Tasks;
    using HtmlAgilityPack;

    public class HtmlHandler : TextHandler
    {
        private readonly Func<HtmlDocument, Task> func;

        public HtmlHandler(Action<HtmlDocument> act) : this(FuncItAsync(act))
        {
        }

        public HtmlHandler(Func<HtmlDocument, Task> func) => this.func = func;

        public override Task Invoke(string value)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(value);
            return this.func(doc);
        }

        private static Func<T, Task> FuncItAsync<T>(Action<T> action)
        {
            return x =>
            {
                action(x);
                return Task.CompletedTask;
            };
        }
    }
}